package com.softech.predict360.survey.extension.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesUtil {
	private static Logger log = Logger.getLogger(PropertiesUtil.class);

	private static Properties properties = new Properties();

	static {
		try {
			InputStream inputStream = RSAWGeneratorRESTClient.class
					.getClassLoader().getResourceAsStream("predict360.properties");

			if(inputStream == null) {
				inputStream = RSAWGeneratorRESTClient.class
						.getClassLoader().getResourceAsStream(
								"application.properties");
			}
			properties.load(inputStream);
		} catch (IOException e) {
			log.error("Error retreiving properties file --->"+e.getMessage());
			e.printStackTrace();
		}
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
}
