package com.softech.predict360.survey.extension.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class RSAWGeneratorRESTClient {

	private static Logger log = Logger.getLogger(RSAWGeneratorRESTClient.class);

	public static Map<Long, List<Long>> loadStandardRequirements(Long surveyId) {

		String url = PropertiesUtil.getProperty("predict.extension.baseUrl")
				+ "/ws/restapi/extension/standardrequirements/" + surveyId;

		DefaultHttpClient httpclient = new DefaultHttpClient();

		HttpGet httpget = new HttpGet(url);

		try {
			HttpResponse response = httpclient.execute(httpget);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String json = reader.readLine();

			httpclient.getConnectionManager().shutdown();

			@SuppressWarnings("unchecked")
			Map<Long, List<Long>> standardRequirements = JSONObject
					.fromObject(json);

			return standardRequirements;
		} catch (IOException e) {
			log.error("RSAWGeneratorRESTClient -> loadStandardRequirements ->"
					+ e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	public static Long loadSurveyCreaterUser(Long surveyId) {

		Long userId = null;
		String url = PropertiesUtil.getProperty("predict.extension.baseUrl")
				+ "/ws/restapi/extension/createruser/" + surveyId;
		DefaultHttpClient httpclient = new DefaultHttpClient();

		HttpGet httpget = new HttpGet(url);

		try {
			HttpResponse response = httpclient.execute(httpget);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));
			String json = reader.readLine();

			httpclient.getConnectionManager().shutdown();

			JSONObject user = JSONObject.fromObject(json);
			userId = user.getLong("createrUserId");

			return userId;
		} catch (IOException e) {
			log.error("RSAWGeneratorRESTClient -> loadSurveyCreaterUser ->"
					+ e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	public static void createCustomAccordion(
			List<HashMap<Object, Object>> customAccordionDetails) {

		String url = PropertiesUtil.getProperty("predict.extension.baseUrl")
				+ "/ws/restapi/extension/createcustomaccordion";

		DefaultHttpClient httpclient = new DefaultHttpClient();

		try {
			HttpPost httpPost = new HttpPost(url);

			StringEntity params = new StringEntity(JSONSerializer.toJSON(
					customAccordionDetails).toString());
			httpPost.addHeader("content-type", "application/json");
			httpPost.setEntity(params);

			HttpResponse response = httpclient.execute(httpPost);

			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			System.out.println(responseString);
		} catch (IOException e) {
			log.error("RSAWGeneratorRESTClient -> createCustomAccordion ->"
					+ e.getMessage());
			e.printStackTrace();
		}
	}

	public static void uploadFile(File file, String fileName, Long surveyId) {

		String url = PropertiesUtil.getProperty("predict.extension.baseUrl")
				+ "/ws/restapi/extension/upload";

		HttpClient client = new DefaultHttpClient();
		HttpPost postRequest = new HttpPost(url);
		try {

			// Set various attributes
			MultipartEntity multiPartEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);

			// Prepare payload
			multiPartEntity.addPart(file.getName(), new FileBody(file));
			multiPartEntity.addPart("fileName", new StringBody(fileName));
			multiPartEntity.addPart("surveyId",
					new StringBody(surveyId.toString()));

			// Set to request body
			postRequest.setEntity(multiPartEntity);

			// Send request
			HttpResponse response = client.execute(postRequest);

			// Verify response if any
			if (response != null) {
				System.out.println(response.getStatusLine().getStatusCode());
			}
		} catch (Exception ex) {
			log.error("RSAWGeneratorRESTClient -> uploadFile ->"
					+ ex.getMessage());
			ex.printStackTrace();
		}
	}
}
