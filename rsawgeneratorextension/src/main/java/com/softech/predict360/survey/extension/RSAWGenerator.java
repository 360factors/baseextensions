package com.softech.predict360.survey.extension;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Map;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.log4j.Logger;

import com.softech.predict360.annotation.ExtensionType;
import com.softech.predict360.extension.spi.Extension;
import com.softech.predict360.survey.extension.util.PropertiesUtil;
import com.softech.predict360.survey.extension.util.RSAWGeneratorRESTClient;

@ExtensionType(code = "rsaw", description = "Generate RSAW", moduleId=5)
public class RSAWGenerator implements Extension {

	private static Logger log = Logger.getLogger(RSAWGenerator.class);

	//@Override
	public int execute(Map<String, Object> params) {

		String reportName = "com/softech/predict360/survey/report/RSAWReport.jrxml";
		
		/*InputStream template = RSAWGenerator.class
				.getResourceAsStream("/report/RSAWReport.jrxml");*/
		
		// compile the report from the stream
		JasperReport report;
		try {
			
			ClassLoader classLoader = getClass().getClassLoader();
			InputStream template = classLoader.getResourceAsStream(reportName);
			
			report = JasperCompileManager.compileReport(template);

			String dbPath = PropertiesUtil.getProperty("predict.extension.dbPath");
			String dbName = PropertiesUtil.getProperty("predict.extension.dbName");
			String dbUser = PropertiesUtil.getProperty("predict.extension.dbUser");
			String dbPassword = PropertiesUtil.getProperty("predict.extension.dbPassword");
			String baseUrl = PropertiesUtil.getProperty("predict.extension.baseUrl");
			String tempDirLoc = PropertiesUtil.getProperty("predict.file.store.tmpdir");

			Connection jdbcConnection = connectDB(dbPath + "/" + dbName,
					dbUser, dbPassword);

			Long surveyId = 0l;
			
			if(params.containsKey("surveyId")){
				surveyId = Long.valueOf(params.get("surveyId").toString());
			}
			
			if(surveyId != 0l){
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("surveyId", surveyId);
				map.put("baseUrl", baseUrl);
				// fill out the report into a print object, ready for export.

				JasperPrint print = JasperFillManager.fillReport(report, map,
						jdbcConnection);

				// export it!
				File pdfFile = File.createTempFile((tempDirLoc != null ? tempDirLoc
						: "") + "RSAWDoc" + System.currentTimeMillis(), "pdf");

				JasperExportManager.exportReportToPdfStream(print,
						new FileOutputStream(pdfFile));

				RSAWGeneratorRESTClient.uploadFile(pdfFile, "RSAW.pdf", surveyId);
			}			

		} catch (JRException e) {
			log.error("RSAWGenerator --> execute -->  error --> "+e.getMessage());
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			log.error("RSAWGenerator --> execute -->  error --> "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error("RSAWGenerator --> execute -->  error --> "+e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			log.error("RSAWGenerator --> execute -->  error --> "+e.getMessage());
			e.printStackTrace();
		}

		System.out.println("RSAW Generator functionality executed");

		return 0;
	}

	/**
	 * Takes 3 parameters: databaseName, userName, password and connects to the
	 * database.
	 * 
	 * @param databaseName
	 *            holds database name,
	 * @param userName
	 *            holds user name
	 * @param password
	 *            holds password to connect the database,
	 * @return Returns the JDBC connection to the database
	 */
	public static Connection connectDB(String databaseName, String userName,
			String password) {
		Connection jdbcConnection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			jdbcConnection = DriverManager.getConnection(databaseName,
					userName, password);
		} catch (Exception ex) {
			log.error("RSAWGenerator --> execute --> "+ex.getMessage());
			ex.printStackTrace();
		}
		return jdbcConnection;
	}

}
