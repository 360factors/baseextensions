package com.softech.predict360.ofac.extension;

import java.util.Map;

import org.apache.log4j.Logger;

import com.softech.predict360.annotation.ExtensionType;
import com.softech.predict360.extension.service.ExtensionService;
import com.softech.predict360.extension.spi.Extension;

@ExtensionType(code = "ofac", description = "OFAC", moduleId=17)
public class Ofac implements Extension {

	private static Logger log = Logger.getLogger(Ofac.class);

	public int execute(Map<String, Object> params) {
		log.debug(">> in Ofac::execute");		
		try {
			ExtensionService.getInstance().printParams(params);
		} catch (Exception e) {
			log.error("Ofac --> execute --> error --> "+e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}

}
