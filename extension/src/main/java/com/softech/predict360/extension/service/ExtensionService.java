package com.softech.predict360.extension.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ServiceLoader;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.softech.predict360.annotation.ExtensionType;
import com.softech.predict360.extension.spi.Extension;

public class ExtensionService {
	private static ExtensionService dynamicFunctionalityService;
	private ServiceLoader<Extension> loader;

	public ExtensionService() {
		setLoader(ServiceLoader.load(Extension.class));
	}

    public static synchronized ExtensionService getInstance() {
        if (dynamicFunctionalityService == null) {
        	dynamicFunctionalityService = new ExtensionService();
        }
        return dynamicFunctionalityService;
    }

	public HashMap<String, String> loadFunctionalities() {
		HashMap<String, String> functionalitiesList = new HashMap<String, String>();

		Iterator<Extension> functionalities = loader.iterator();
		while (functionalities.hasNext()) {
			Extension functionality = functionalities.next();
			ExtensionType functionalityType = functionality.getClass()
					.getAnnotation(ExtensionType.class);
			functionalitiesList.put(functionalityType.code(), functionalityType.description());
		}

		return functionalitiesList;
	}

	
	public JsonArray getAllExtensions() {
		List<ExtensionType> ExtensionTypes = new ArrayList<>();
		Iterator<Extension> functionalities = loader.iterator();
		while (functionalities.hasNext()) {
			Extension functionality = functionalities.next();
			ExtensionType functionalityType = functionality.getClass().getAnnotation(ExtensionType.class);
			ExtensionTypes.add(functionalityType);			 
		}
		
		Gson gson = new Gson();
		//GsonBuilder gsonBuilder = new GsonBuilder().;
		JsonArray jsonArray = gson.toJsonTree(ExtensionTypes).getAsJsonArray();
		return jsonArray;
	}
	
	public HashMap<String, String> loadFunctionalities(List<Long> moduleIds) {
		HashMap<String, String> functionalitiesList = new HashMap<String, String>();

		Iterator<Extension> functionalities = loader.iterator();
		while (functionalities.hasNext()) {
			Extension functionality = functionalities.next();
			ExtensionType functionalityType = functionality.getClass().getAnnotation(ExtensionType.class);
			if(moduleIds.contains(Long.valueOf(functionalityType.moduleId()))){
				functionalitiesList.put(functionalityType.code(), functionalityType.description());
			}			
		}
		
		return functionalitiesList;
	}
	
	public HashMap<String, Object> loadFunctionaliesModule() {
		HashMap<String, Object> functionalitiesList = new HashMap<String, Object>();

		Iterator<Extension> functionalities = loader.iterator();
		while (functionalities.hasNext()) {
			Extension functionality = functionalities.next();
			ExtensionType functionalityType = functionality.getClass()
					.getAnnotation(ExtensionType.class);
			functionalitiesList.put(functionalityType.code(), functionalityType.moduleId());
		}

		return functionalitiesList;
	}

	public int executeFunctionality(String functionalityUniqueCode, Map<String,Object> params) {
		int resultCode = -1;

		Iterator<Extension> functionalities = loader.iterator();
		while (functionalities.hasNext()) {
			Extension functionality = functionalities.next();
			ExtensionType functionalityType = functionality.getClass()
					.getAnnotation(ExtensionType.class);

			if (functionalityType.code().equals(functionalityUniqueCode)) {
				resultCode = functionality.execute(params);
				break;
			}
		}

		return resultCode;
	}

	public void printParams(Map<String,Object> params){
		Iterator<Entry<String, Object>> iter = params.entrySet().iterator();
		System.out.println("params:");
        while (iter.hasNext()) {
            Entry<String, Object> entry = iter.next();
            System.out.println("key="+entry.getKey()+", value="+entry.getValue());
        }		
	}
	
	public ServiceLoader<Extension> getLoader() {
		return loader;
	}

	public void setLoader(ServiceLoader<Extension> loader) {
		this.loader = loader;
	}
}