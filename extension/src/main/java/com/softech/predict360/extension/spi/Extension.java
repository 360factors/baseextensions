package com.softech.predict360.extension.spi;

import java.util.Map;

public interface Extension {
	public int execute(Map<String,Object> params);
}
