package com.softech.predict360.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ExtensionType {
	public String code(); 
	public String description();
	public long moduleId();
}
