package com.softech.predict360.rdc.extension;

import java.util.Map;

import org.apache.log4j.Logger;

import com.softech.predict360.annotation.ExtensionType;
import com.softech.predict360.extension.service.ExtensionService;
import com.softech.predict360.extension.spi.Extension;

@ExtensionType(code = "rdc", description = "RDC", moduleId=17)
public class Rdc implements Extension {

	private static Logger log = Logger.getLogger(Rdc.class);
	
	public int execute(Map<String, Object> params) {
		log.debug(">> in Rdc::execute");		
		try {
			ExtensionService.getInstance().printParams(params);
		} catch (Exception e) {
			log.error("Rdc --> execute --> error --> "+e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}

}
