package com.softech.predict360.kroll.extension;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Map;

import org.apache.log4j.Logger;

import com.softech.predict360.annotation.ExtensionType;
import com.softech.predict360.extension.service.ExtensionService;
import com.softech.predict360.extension.spi.Extension;

@ExtensionType(code = "kroll", description = "KROLL", moduleId=17)
public class Kroll implements Extension { 

	private static Logger log = Logger.getLogger(Kroll.class);

	public int execute(Map<String, Object> params) {
		log.debug(">> in Kroll::execute");		
		try {
			ExtensionService.getInstance().printParams(params);
		} catch (Exception e) {
			log.error("Kroll --> execute --> error --> "+e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Takes 3 parameters: databaseName, userName, password and connects to the
	 * database.
	 * 
	 * @param databaseName
	 *            holds database name,
	 * @param userName
	 *            holds user name
	 * @param password
	 *            holds password to connect the database,
	 * @return Returns the JDBC connection to the database
	 */
	public static Connection connectDB(String databaseName, String userName,
			String password) {
		Connection jdbcConnection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			jdbcConnection = DriverManager.getConnection(databaseName,
					userName, password);
		} catch (Exception ex) {
			log.error("Kroll --> execute --> "+ex.getMessage());
			ex.printStackTrace();
		}
		return jdbcConnection;
	}
}
